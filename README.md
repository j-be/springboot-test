# springboot-test

Some random shenanigan with Spring Boot

## Assumptions

* a folder's name (e.g. when sorting) is only its name, not the full path
* scan date is updated each time a file is scanned
* scanning is fully recursive, and the root folder's files are included, too.
* the aggregated size of a folder only consists of its own content, and does NOT include the content of its subfolders

## Building

* `mvn clean package`

## Other noteworthy stuff

* The `@SpringBootApplication` class was put into the `rest` package. Never used Spring before, so not sure where it
  traditionally goes.
* Initially, a `@EventListener(ApplicationReadyEvent.class)` was implemented to scan at startup. However, Quartz
  schedules the task on startup anyway, so it was removed again.
* SpringBoot seems to come with a Tomcat by default, so that was used.
* In-memory H2 is used for ease of use. Before going to prod one should switch to e.g. Postgres and FlyWay.
  * This also means the Docker stuff is currently for reference only.
* It was a bit of a pain to get `@Autowired` working in the test. I assume there is an easier way to do this, didn't
  find it yet though.
* I didn't find a way to set `current working directory` dynamically within the tests. Hence the dirty "make it public"
  hack. This should be removed.
* `rest` module is mostly untested. Doing some `RestAssured` before going to prod may ba a good idea.

## Tasks

1. Build a Maven project with at least 2 modules and a parent
  * [x] core = contains services
  * [x] rest = rest api
2. Build a Spring Boot Rest API
3. run as self containing single Spring Boot jar with e.g. jetty embedded
4. Design a Service
  * [x] reading/scanning all files and subfolders of a specific root folder (configure in application.properties/yaml)
  * [x] writing file metadata (path, filename, filetype=extension, filesize, modification date, scan date=now) into a persisted database (h2, postgreSQL, ...)
  * [x] run on startup
  * [x] nice to have: scheduling all x minutes and rescan the folder and rewrite/update files or drop and reinsert (x .. configureable)
5. Rest ApI:
  * [x] `GET /folders`
    * delivers a list of folders and subfolders sorted by name
  * [x] `GET /filesizes?filetype=xxx`
    * delivers folders and subfolders with aggregated filesize sorted by size
    * filtered by filetype/extension=xxx if avail
  * [x] `DELETE /`
    * purge all files and folder from db via rest api
6. Add some unit tests  
7. Nice:
  * [x] docker-compose running service and database
