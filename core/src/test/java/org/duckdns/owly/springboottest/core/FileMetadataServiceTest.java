package org.duckdns.owly.springboottest.core;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;

import javax.persistence.EntityManager;
import java.io.File;
import java.nio.file.Path;

@DataJpaTest
@SpringJUnitConfig
public class FileMetadataServiceTest {
    private static final Path rootDirectory = new File("src/test/resources/test-files").getAbsoluteFile().toPath();

    @TestConfiguration
    static class EmployeeServiceImplTestContextConfiguration {
        @Bean
        public FileMetadataService fileMetadataService() {
            return new FileMetadataService();
        }
    }

    @Autowired
    private FileMetadataService fileMetadataService;
    @Autowired
    private FileMetadataRepository fileMetadataRepository;
    @Autowired
    private EntityManager entityManager;

    @Test
    public void testBasic() {
        fileMetadataService.rootDirectory = rootDirectory;
        Assertions.assertTrue(fileMetadataService.getAllFolders().isEmpty());
        Assertions.assertTrue(fileMetadataService.getAllFoldersWithSize().isEmpty());
        Assertions.assertTrue(fileMetadataService.getAllFoldersWithSize("txt").isEmpty());
        fileMetadataService.scanFolder();
        Assertions.assertEquals(4, fileMetadataService.getAllFolders().size());
        Assertions.assertEquals(4, fileMetadataService.getAllFoldersWithSize().size());
        Assertions.assertEquals(3, fileMetadataService.getAllFoldersWithSize("txt").size());
        fileMetadataService.purge();
        Assertions.assertTrue(fileMetadataService.getAllFolders().isEmpty());
        Assertions.assertTrue(fileMetadataService.getAllFoldersWithSize().isEmpty());
        Assertions.assertTrue(fileMetadataService.getAllFoldersWithSize("txt").isEmpty());
    }

    @Test
    public void testSortingOnName() {
        fileMetadataService.rootDirectory = rootDirectory;
        fileMetadataService.scanFolder();

        final var folders = fileMetadataService.getAllFolders();
        Assertions.assertEquals("another_folder", folders.get(0).getName());
        Assertions.assertEquals("some-folder/another_folder", folders.get(0).getFullPath());
        Assertions.assertEquals("nested-folder", folders.get(1).getName());
        Assertions.assertEquals("some-folder/nested-folder", folders.get(1).getFullPath());
        Assertions.assertEquals("some-folder", folders.get(2).getName());
        Assertions.assertEquals("some-folder", folders.get(2).getFullPath());
        Assertions.assertEquals("test-files", folders.get(3).getName());
        Assertions.assertEquals("", folders.get(3).getFullPath());
    }

    @Test
    public void testSortingOnSize() {
        fileMetadataService.rootDirectory = rootDirectory;
        fileMetadataService.scanFolder();

        final var folders = fileMetadataService.getAllFoldersWithSize();
        Assertions.assertEquals("some-folder", folders.get(0).getName());
        Assertions.assertEquals("some-folder", folders.get(0).getFullPath());
        Assertions.assertEquals(47, folders.get(0).getAggregatedSize());
        Assertions.assertEquals("nested-folder", folders.get(1).getName());
        Assertions.assertEquals("some-folder/nested-folder", folders.get(1).getFullPath());
        Assertions.assertEquals(55, folders.get(1).getAggregatedSize());
        Assertions.assertEquals("test-files", folders.get(2).getName());
        Assertions.assertEquals("", folders.get(2).getFullPath());
        Assertions.assertEquals(128, folders.get(2).getAggregatedSize());
        Assertions.assertEquals("another_folder", folders.get(3).getName());
        Assertions.assertEquals("some-folder/another_folder", folders.get(3).getFullPath());
        Assertions.assertEquals(435, folders.get(3).getAggregatedSize());
    }

    @Test
    public void testSortingOnSizeWithFilter() {
        fileMetadataService.rootDirectory = rootDirectory;
        fileMetadataService.scanFolder();

        {
            final var folders = fileMetadataService.getAllFoldersWithSize("txt");
            Assertions.assertEquals("nested-folder", folders.get(0).getName());
            Assertions.assertEquals("some-folder/nested-folder", folders.get(0).getFullPath());
            Assertions.assertEquals(5, folders.get(0).getAggregatedSize());
            Assertions.assertEquals("test-files", folders.get(1).getName());
            Assertions.assertEquals("", folders.get(1).getFullPath());
            Assertions.assertEquals(113, folders.get(1).getAggregatedSize());
            Assertions.assertEquals("another_folder", folders.get(2).getName());
            Assertions.assertEquals("some-folder/another_folder", folders.get(2).getFullPath());
            Assertions.assertEquals(430, folders.get(2).getAggregatedSize());
        }

        {
            final var folders = fileMetadataService.getAllFoldersWithSize("wow");
            Assertions.assertEquals("another_folder", folders.get(0).getName());
            Assertions.assertEquals("some-folder/another_folder", folders.get(0).getFullPath());
            Assertions.assertEquals(5, folders.get(0).getAggregatedSize());
            Assertions.assertEquals("nested-folder", folders.get(1).getName());
            Assertions.assertEquals("some-folder/nested-folder", folders.get(1).getFullPath());
            Assertions.assertEquals(50, folders.get(1).getAggregatedSize());
        }
    }

    @Test
    public void testLongExtensions() {
        fileMetadataService.rootDirectory = rootDirectory;
        fileMetadataService.scanFolder();

        final var folders = fileMetadataService.getAllFoldersWithSize("extension");
        Assertions.assertEquals(1, folders.size());
        Assertions.assertEquals("some-folder", folders.get(0).getName());
        Assertions.assertEquals("some-folder", folders.get(0).getFullPath());
    }

    @Test
    public void testRescanOnFolders() {
        fileMetadataService.rootDirectory = rootDirectory;
        fileMetadataService.scanFolder();
        final var before = fileMetadataService.getAllFolders();
        fileMetadataService.scanFolder();
        final var after = fileMetadataService.getAllFolders();

        Assertions.assertEquals(before.size(), after.size());
        for (int i = 0; i < before.size(); i++) {
            Assertions.assertNotSame(before, after);
            Assertions.assertEquals(before.get(i).getId(), after.get(i).getId());
            Assertions.assertEquals(before.get(i).getName(), after.get(i).getName());
            Assertions.assertEquals(before.get(i).getFullPath(), after.get(i).getFullPath());
        }
    }

    @Test
    public void testRescanOnFiles() {
        fileMetadataService.rootDirectory = rootDirectory;
        fileMetadataService.scanFolder();
        final var before = fileMetadataRepository.findAll().iterator();
        before.forEachRemaining(entityManager::detach);
        fileMetadataService.scanFolder();
        final var after = fileMetadataRepository.findAll().iterator();

        while(before.hasNext()) {
            final var beforeItem = before.next();
            final var afterItem = after.next();
            Assertions.assertNotSame(beforeItem, afterItem);
            Assertions.assertEquals(beforeItem.getId(), afterItem.getId());
            Assertions.assertEquals(beforeItem.getFilename(), afterItem.getFilename());
            Assertions.assertEquals(beforeItem.getFilesize(), afterItem.getFilesize());
            Assertions.assertEquals(beforeItem.getFiletype(), afterItem.getFiletype());
            Assertions.assertEquals(beforeItem.getFolder().getId(), afterItem.getFolder().getId());
            Assertions.assertEquals(beforeItem.getModificationDate(), afterItem.getModificationDate());
            Assertions.assertEquals(beforeItem.getPath(), afterItem.getPath());
            Assertions.assertTrue(beforeItem.getScanDate().isBefore(afterItem.getScanDate()));
            Assertions.assertEquals(before.hasNext(), after.hasNext());
        }
    }

    @SpringBootApplication
    static class TestApplication {
    }
}
