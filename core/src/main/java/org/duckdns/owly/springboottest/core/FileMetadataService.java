package org.duckdns.owly.springboottest.core;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Sort;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.TimeUnit;
import java.util.stream.Stream;

@Slf4j
@Service
public class FileMetadataService {

    @Autowired
    private FolderRepository folderRepository;
    @Autowired
    private FileMetadataRepository fileMetadataRepository;
    @Autowired
    private EntityManager entityManager;

    // TODO: I'm not able to get this patched in tests
    @Value("${root_directory}")
    public Path rootDirectory;

    @Scheduled(fixedRateString = "${scan_interval}", timeUnit = TimeUnit.MINUTES)
    public void scanFolder() {
        if (!rootDirectory.toFile().isDirectory()) {
            log.error("The given path '" + this.rootDirectory + "' is not a directory. Refusing to do anything.");
            return;
        }

        // Blatantly stolen from https://www.javaprogramto.com/2019/12/java-list-all-files-recursively.html
        // Did adapt it to my needs though
        try (Stream<Path> walk = Files.walk(rootDirectory)) {
            walk.forEach(this::handle);
        } catch (IOException e) {
            log.error("Caught exception while traversing '" + this.rootDirectory + "'", e);
        }
    }

    private void handle(Path path) {
        if (path.toFile().isDirectory()) {
            final Folder folder = this.createOrUpdateDirectory(path);

            try (Stream<Path> walk = Files.walk(path, 1)) {
                walk.filter(p -> p.toFile().isFile())
                        .forEach(p -> createOrUpdateMetadata(p, folder));
            } catch (IOException e) {
                log.error("Caught exception while traversing '" + this.rootDirectory + "'", e);
            }
        }
    }

    private Folder createOrUpdateDirectory(Path path) {
        if (!path.toFile().isDirectory()) {
            log.error("Refusing to writeOrUpdateDirectory on '" + path + "'. Not a directory.");
            return null;
        }

        final String relativePath = rootDirectory.relativize(path).toString();
        final Optional<Folder> existingFolder = folderRepository.findByFullPath(relativePath);
        if (existingFolder.isPresent())
            return existingFolder.get();

        final Folder newFolder = new Folder();
        newFolder.setName(path.getFileName().toString());
        newFolder.setFullPath(relativePath);
        log.info("Saving " + newFolder);
        return folderRepository.save(newFolder);
    }

    private FileMetadata createOrUpdateMetadata(Path path, Folder folder) {
        if (!path.toFile().isFile()) {
            log.error("Refusing to writeOrUpdateMetadata on '" + path + "'. Not a file.");
            return null;
        }

        final String relativePath = rootDirectory.relativize(path).toString();

        final FileMetadata metadata = fileMetadataRepository.findByPath(relativePath).orElseGet(() -> {
            final var newMetadata = new FileMetadata();
            newMetadata.setFilename(path.getFileName().toString());
            newMetadata.setPath(relativePath);
            newMetadata.setFiletype(Utils.getExtension(path));
            newMetadata.setFolder(folder);
            return newMetadata;
        });

        metadata.setFilesize(path.toFile().length());
        metadata.setModificationDate(Utils.getLastModified(path));
        metadata.setScanDate(LocalDateTime.now());

        log.info("Saving " + metadata);
        return fileMetadataRepository.save(metadata);
    }

    public List<Folder> getAllFolders() {
        return folderRepository.findAll(Sort.by("name"));
    }

    public List<FolderWithSize> getAllFoldersWithSize() {
        return entityManager.createQuery(
                        "select new org.duckdns.owly.springboottest.core.FolderWithSize(" +
                                "  f.id," +
                                "  f.name," +
                                "  f.fullPath," +
                                "  SUM(md.filesize)" +
                                ") " +
                                "from FileMetadata md " +
                                "  inner join md.folder f " +
                                "group by f.id, f.name, f.fullPath " +
                                "order by 4",
                        FolderWithSize.class)
                .getResultList();
    }

    public List<FolderWithSize> getAllFoldersWithSize(String filetype) {
        final TypedQuery<FolderWithSize> query = entityManager.createQuery(
                "select new org.duckdns.owly.springboottest.core.FolderWithSize(" +
                        "  f.id," +
                        "  f.name," +
                        "  f.fullPath," +
                        "  SUM(md.filesize)" +
                        ") " +
                        "from FileMetadata md " +
                        "  inner join md.folder f " +
                        "where md.filetype = :filetype " +
                        "group by f.id, f.name, f.fullPath " +
                        "order by 4",
                FolderWithSize.class);
        query.setParameter("filetype", filetype);
        return query.getResultList();
    }

    public void purge() {
        fileMetadataRepository.deleteAll();
        folderRepository.deleteAll();
    }
}
