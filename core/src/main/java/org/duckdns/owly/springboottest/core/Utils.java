package org.duckdns.owly.springboottest.core;

import lombok.experimental.UtilityClass;

import java.nio.file.Path;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;

@UtilityClass
public class Utils {
    public String getExtension(Path path) {
        if (!path.toFile().isFile())
            return "";
        if (!path.toString().contains("."))
            return "";
        // Blatantly stolen from https://www.baeldung.com/java-file-extension
        final String fileName = path.toString();
        return fileName.substring(fileName.lastIndexOf(".") + 1);
    }

    public static LocalDateTime getLastModified(Path path) {
        final long milliseconds = path.toFile().lastModified();
        final LocalDateTime now = LocalDateTime.now();
        final ZoneId systemZone = ZoneId.systemDefault();

        return LocalDateTime.ofEpochSecond(
                milliseconds / 1000,
                Long.valueOf(milliseconds % 1000).intValue() * 1_000_000,
                systemZone.getRules().getOffset(now)
        );
    }
}
