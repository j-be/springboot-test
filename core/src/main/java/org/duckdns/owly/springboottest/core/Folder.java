package org.duckdns.owly.springboottest.core;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Entity
@Table(
        uniqueConstraints = {
                @UniqueConstraint(columnNames = "fullPath"),
        },
        indexes = {
                @Index(columnList = "fullPath"),
        }
)
@Getter
@Setter
@ToString
public class Folder {
    @Id
    @GeneratedValue
    private Long id;
    @NotBlank
    private String name;
    @NotNull
    private String fullPath;
}
