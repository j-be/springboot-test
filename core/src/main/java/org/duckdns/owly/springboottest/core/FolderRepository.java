package org.duckdns.owly.springboottest.core;

import org.springframework.data.domain.Sort;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

interface FolderRepository extends CrudRepository<Folder, Long> {
    Optional<Folder> findByFullPath(String fullPath);
    List<Folder> findAll(Sort sort);
}
