package org.duckdns.owly.springboottest.core;

import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

interface FileMetadataRepository extends CrudRepository<FileMetadata, Long> {
    Optional<FileMetadata> findByPath(String path);
}
