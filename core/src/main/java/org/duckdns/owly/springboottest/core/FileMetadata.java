package org.duckdns.owly.springboottest.core;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Entity
@Table(
        uniqueConstraints = {
                @UniqueConstraint(columnNames = "path"),
        },
        indexes = {
                @Index(columnList = "path"),
                @Index(columnList = "filetype"),
        }
)
@Getter
@Setter
@ToString
public class FileMetadata {
    @Id
    @GeneratedValue
    private Long id;

    @NotBlank
    private String path;
    @NotBlank
    private String filename;
    private String filetype;
    private long filesize;
    @NotNull
    private LocalDateTime modificationDate;
    @NotNull
    private LocalDateTime scanDate;

    @ManyToOne
    private Folder folder;
}
