package org.duckdns.owly.springboottest.core;

import lombok.Value;

@Value
public class FolderWithSize {
    Long id;
    String name;
    String fullPath;
    long aggregatedSize;
}
