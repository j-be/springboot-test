package org.duckdns.owly.springboottest.rest;

import lombok.extern.slf4j.Slf4j;
import org.duckdns.owly.springboottest.core.FileMetadataService;
import org.duckdns.owly.springboottest.core.Folder;
import org.duckdns.owly.springboottest.core.FolderWithSize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
public class FolderController {
    @Autowired
    private FileMetadataService fileMetadataService;

    @GetMapping("/folders")
    public Iterable<Folder> all() {
        return fileMetadataService.getAllFolders();
    }

    @GetMapping("/filesizes")
    public Iterable<FolderWithSize> foldersWithSize(@RequestParam(value = "filetype", required = false) String filetype) {
        if (filetype == null)
            return fileMetadataService.getAllFoldersWithSize();
        return fileMetadataService.getAllFoldersWithSize(filetype);
    }

    @DeleteMapping("/")
    public void deleteEmployee() {
        fileMetadataService.purge();
    }
}
